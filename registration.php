

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Bootstrap, from Twitter</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
        body {
            padding-top: 40px;
            padding-bottom: 40px;
            background-color: #f5f5f5;
        }

        .registration {
            max-width: 350px;
            padding: 19px 29px 29px;
            margin: 0 auto 20px;
            background-color: #fff;
            border: 1px solid #e5e5e5;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
            -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
            -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
            box-shadow: 0 1px 2px rgba(0,0,0,.05);
        }
        .registration .registration-heading {
            margin-bottom: 10px;
        }
        .registration input[type="text"],
        .registration input[type="paswoord"] {
            font-size: 16px;
            height: auto;
            margin-bottom: 15px;
            padding: 7px 9px;
        }

        .registration label.error {
            color: red;
        }
    </style>
    <link href="css/bootstrap-responsive.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="assets/ico/favicon.png">
</head>

<body>

<div class="container">

    <form class="registration" action="registrationfunction.php" method="post">
        <h2 class="registration-heading">Please sign in</h2>

        <?php if (isset($errors['email'])) { ?>
        <label class="error" for="email"><?php echo $errors['email']; ?></label>
        <?php } ?>
        <input type="text" class="input-block-level" name="email" id="email" placeholder="Email address" value="<?php echo $email; ?>">



        <?php if (isset($errors['gebruikersnaam'])) { ?>
        <label class="error" for="gebruikersnaam"><?php echo $errors['gebruikersnaam']; ?></label>
        <?php } ?>
        <input type="text" class="input-block-level" name="gebruikersnaam" id="gebruikersnaam" placeholder="Username">

        <?php if (isset($errors['naam'])) { ?>
        <label class="error" for="naam"><?php echo $errors['naam']; ?></label>
        <?php } ?>
        <input type="text" class="input-block-level" name="naam" id="naam" placeholder="naam">

        <?php if (isset($errors['voornaam'])) { ?>
        <label class="error" for="voornaam"><?php echo $errors['voornaam']; ?></label>
        <?php } ?>
        <input type="text" class="input-block-level" name="voornaam" id="voornaam" placeholder="voornaam">


        <?php if (isset($errors['paswoord'])) { ?>
        <label class="error" for="paswoord"><?php echo $errors['paswoord']; ?></label>
        <?php } ?>
        <input type="paswoord" class="input-block-level" name="paswoord" id="paswoord" placeholder="Paswoord">

        <?php if (isset($errors['confirmed-paswoord'])) { ?>
        <label class="error" for="confirmed-paswoord"><?php echo $errors['confirmed-paswoord']; ?></label>
        <?php } ?>
        <input type="paswoord" class="input-block-level" name="confirmed-paswoord" id="confirmed-paswoord" placeholder="Enter your password for confirmation">

        <?php if (isset($errors['straat'])) { ?>
        <label class="error" for="straat"><?php echo $errors['straat']; ?></label>
        <?php } ?>
        <input type="text" class="input-block-level" name="straat" id="straat" placeholder="Straat" value="<?php echo $straat; ?>">

        <?php if (isset($errors['postcode'])) { ?>
        <label class="error" for="postcode"><?php echo $errors['postcode']; ?></label>
        <?php } ?>
        <input type="text" class="input-block-level" name="postcode" id="postcode" placeholder="postcode" value="<?php echo $postcode; ?>">

        <?php if (isset($errors['gemeente'])) { ?>
        <label class="error" for="gemeente"><?php echo $errors['gemeente']; ?></label>
        <?php } ?>
        <input type="text" class="input-block-level" name="gemeente" id="gemeente" placeholder="gemeente" value="<?php echo $gemeente; ?>">

        <?php if (isset($errors['land'])) { ?>
        <label class="error" for="land"><?php echo $errors['land']; ?></label>
        <?php } ?>
        <input type="text" class="input-block-level" name="land" id="land" placeholder="land" value="<?php echo $land; ?>">

        <?php if (isset($errors['telefoon'])) { ?>
        <label class="error" for="telefoon"><?php echo $errors['telefoon']; ?></label>
        <?php } ?>
        <input type="text" class="input-block-level" name="telefoon" id="telefoon" placeholder="telefoon" value="<?php echo $telefoon; ?>">



        <?php if (isset($errors['straat levering'])) { ?>
            <label class="error" for="straat levering"><?php echo $errors['straat levering']; ?></label>
        <?php } ?>
        <input type="text" class="input-block-level" name="straat levering" id="straat levering" placeholder="straat levering" value="<?php echo $straat_levering; ?>">

        <?php if (isset($errors['postcode_levering'])) { ?>
            <label class="error" for="postcode_levering"><?php echo $errors['postcode_levering']; ?></label>
        <?php } ?>
        <input type="text" class="input-block-level" name="postcode_levering" id="postcode_levering" placeholder="postcode leverings adres" value="<?php echo $postcode_levering; ?>">

        <?php if (isset($errors['gemeente_levering'])) { ?>
            <label class="error" for="gemeente_levering"><?php echo $errors['gemeente_levering']; ?></label>
        <?php } ?>
        <input type="text" class="input-block-level" name="gemeente_levering" id="gemeente_levering" placeholder="gemeente levering" value="<?php echo $gemeente_levering; ?>">

        <?php if (isset($errors['land_levering'])) { ?>
            <label class="error" for="land_levering"><?php echo $errors['land_levering']; ?></label>
        <?php } ?>
        <input type="text" class="input-block-level" name="land_levering" id="land_levering" placeholder="land levering" value="<?php echo $land_levering; ?>">


        <fieldset>
            <p>

                <select id = "payment">
                    <option value = "1">paypal</option>
                    <option value = "2">overschrijving</option>
                    <option value = "3">bankcontact</option>
                </select>
            </p>
        </fieldset>
        <input type="checkbox" name="nieuwsbrief" value="1" > ik wil op de hoogte gehouden worden via de nieuwsbrief <br />
        <input type="checkbox" name="accept" value="accepted" > Bij deze bevestig ik de Terms & Services <br />



        <button class="btn btn-large btn-primary" value="submit" type="submit">Register</button>
    </form>

</div> <!-- /container -->



<!-- Le javascript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery.js"></script>
<script src="js/bootstrap-transition.js"></script>
<script src="js/bootstrap-alert.js"></script>
<script src="js/bootstrap-modal.js"></script>
<script src="js/bootstrap-dropdown.js"></script>
<script src="js/bootstrap-scrollspy.js"></script>
<script src="js/bootstrap-tab.js"></script>
<script src="js/bootstrap-tooltip.js"></script>
<script src="js/bootstrap-popover.js"></script>
<script src="js/bootstrap-button.js"></script>
<script src="js/bootstrap-collapse.js"></script>
<script src="js/bootstrap-carousel.js"></script>
<script src="js/bootstrap-typeahead.js"></script>

</body>
</html>