<?php
/**
 * Created by JetBrains PhpStorm.
 * User: fredericklemmens
 * Date: 16/04/13
 * Time: 09:45
 * To change this template use File | Settings | File Templates.
 */


namespace Models\Users;

class user {


    protected  $naam;
    protected  $voornaam;
    protected  $gebruikersnaam ;
    protected  $wachtwoord;
    protected  $telefoon;
    protected  $email;
    protected  $leveringsAdres;
    protected  $facturatieAdres;
    protected  $voorkeurBetalingsSysteem;
    protected  $interesseInNieuwsbrief;
    protected  $heeftRegistratieDoorlopen;
    protected  $winkelwagentje;
    protected  $bestellingen;
    protected  $favorieten;
    protected  $isGeblokkeerd;



    public function __constructor () {


    }

    public function setBestellingen($bestellingen)
    {
        $this->bestellingen = $bestellingen;
    }
    public function getBestellingen()
    {
        return $this->bestellingen;
    }
    public function setEmail($email)
    {
        $this->email = $email;
    }
    public function getEmail()
    {
        return $this->email;
    }
    public function setFacturatieAdres($facturatieAdres)
    {
        $this->facturatieAdres = $facturatieAdres;
    }
    public function getFacturatieAdres()
    {
        return $this->facturatieAdres;
    }
    public function setFavorieten($favorieten)
    {
        $this->favorieten = $favorieten;
    }
    public function getFavorieten()
    {
        return $this->favorieten;
    }
    public function setGebruikersnaam($gebruikersnaam)
    {
        $this->gebruikersnaam = $gebruikersnaam;
    }
    public function getGebruikersnaam()
    {
        return $this->gebruikersnaam;
    }
    public function setHeeftRegistratieDoorlopen($heeftRegistratieDoorlopen)
    {
        $this->heeftRegistratieDoorlopen = $heeftRegistratieDoorlopen;
    }
    public function getHeeftRegistratieDoorlopen()
    {
        return $this->heeftRegistratieDoorlopen;
    }
    public function setInteresseInNieuwsbrief($interesseInNieuwsbrief)
    {
        $this->interesseInNieuwsbrief = $interesseInNieuwsbrief;
    }
    public function getInteresseInNieuwsbrief()
    {
        return $this->interesseInNieuwsbrief;
    }
    public function setIsGeblokkeerd($isGeblokkeerd)
    {
        $this->isGeblokkeerd = $isGeblokkeerd;
    }
    public function getIsGeblokkeerd()
    {
        return $this->isGeblokkeerd;
    }
    public function setLeveringsAdres($leveringsAdres)
    {
        $this->leveringsAdres = $leveringsAdres;
    }
    public function getLeveringsAdres()
    {
        return $this->leveringsAdres;
    }
    public function setNaam($naam)
    {
        $this->naam = $naam;
    }
    public function getNaam()
    {
        return $this->naam;
    }
    public function setTelefoon($telefoon)
    {
        $this->telefoon = $telefoon;
    }
    public function getTelefoon()
    {
        return $this->telefoon;
    }
    public function setVoorkeurBetalingsSysteem($voorkeurBetalingsSysteem)
    {
        $this->voorkeurBetalingsSysteem = $voorkeurBetalingsSysteem;
    }
    public function getVoorkeurBetalingsSysteem()
    {
        return $this->voorkeurBetalingsSysteem;
    }
    public function setVoornaam($voornaam)
    {
        $this->voornaam = $voornaam;
    }
    public function getVoornaam()
    {
        return $this->voornaam;
    }
    public function setWachtwoord($wachtwoord)
    {
        $this->wachtwoord = $wachtwoord;
    }
    public function getWachtwoord()
    {
        return $this->wachtwoord;
    }
    public function setWinkelwagentje($winkelwagentje)
    {
        $this->winkelwagentje = $winkelwagentje;
    }
    public function getWinkelwagentje()
    {
        return $this->winkelwagentje;
    }






    public function registration () {




    }

    public function login () {



    }


    public function logout () {



    }
}