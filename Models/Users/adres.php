<?php
/**
 * Created by JetBrains PhpStorm.
 * User: fredericklemmens
 * Date: 18/04/13
 * Time: 11:11
 * To change this template use File | Settings | File Templates.
 */

namespace Models\Users;


class adres  {

    protected $straat;
    protected $nummer;
    protected $postcode;
    protected $gemeente;
    protected $land;



    public function __constructor () {


    }

    public function setGemeente($gemeente)
    {
        $this->gemeente = $gemeente;
    }

    public function getGemeente()
    {
        return $this->gemeente;
    }

    public function setLand($land)
    {
        $this->land = $land;
    }

    public function getLand()
    {
        return $this->land;
    }

    public function setNummer($nummer)
    {
        $this->nummer = $nummer;
    }

    public function getNummer()
    {
        return $this->nummer;
    }

    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;
    }

    public function getPostcode()
    {
        return $this->postcode;
    }

    public function setStraat($straat)
    {
        $this->straat = $straat;
    }

    public function getStraat()
    {
        return $this->straat;
    }





}