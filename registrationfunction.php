<?php




$errors = false;
$email = '';
$straat = '';
$postcode = '';
$gemeente = '';
$land = '';
$telefoon = '';





if (empty ($_POST ['accept'])) {

    //not accept
    echo "no can go";
}
else {


if (isset($_POST['register'])) {
// De gebruiker heeft de login button geklikt.
// Valideer de gegevens
if ($_POST['email'] != '') {
    $_POST['email'] = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);

    if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
        $errors['email'] = 'Please enter a valid email address.';
    }

    // Sla de waarde op in een variabele zodat we dat kunnen tonen als er
    // foutmeldingen zijn.
    $email = $_POST['email'];
} else {
    $errors['email'] = 'Please enter your email address.';
}

if ($_POST['paswoord'] != '') {
    $_POST['paswoord'] = filter_var($_POST['paswoord'], FILTER_SANITIZE_STRING);

    if ($_POST['paswoord'] == '') {
        $errors['paswoord'] = 'Please enter a valid password.';
    }
} else {
    $errors['paswoord'] = 'Please enter your password.';
}





if ($_POST['confirmed-gebruikersnaam'] != '') {
    $_POST['confirmed-gebruikersnaam'] = filter_var($_POST['confirmed-gebruikersnaam'], FILTER_SANITIZE_STRING);

    if ($_POST['confirmed-gebruikersnaam'] == '') {
        $errors['confirmed-gebruikersnaam'] = 'Please enter a valid username.';
    }
} else {
    $errors['confirmed-gebruikersnaam'] = 'Please enter your username for confirmation.';
}

// Het paswoord veld en het bevestigt paswoord horen hetzelfde te zijn.
if ($_POST['gebruikersnaam'] !== $_POST['confirmed-gebruikersnaam']) {
    $errors['confirmed-gebruikersnaam'] = 'The username don\'t match.';
}







if ($_POST['confirmed-paswoord'] != '') {
    $_POST['confirmed-paswoord'] = filter_var($_POST['confirmed-paswoord'], FILTER_SANITIZE_STRING);

    if ($_POST['confirmed-paswoord'] == '') {
        $errors['confirmed-paswoord'] = 'Please enter a valid password.';
    }
} else {
    $errors['confirmed-paswoord'] = 'Please enter your password for confirmation.';
}

// Het paswoord veld en het bevestigt paswoord horen hetzelfde te zijn.
if ($_POST['password'] !== $_POST['confirmed-paswoord']) {
    $errors['confirmed-paswoord'] = 'The passwords don\'t match.';
}

if ($_POST['straat'] != '') {
    $_POST['straat'] = filter_var($_POST['straat'], FILTER_SANITIZE_STRING);

    // Sla de waarde op in een variabele zodat we dat kunnen tonen als er
    // foutmeldingen zijn.
    $street = $_POST['street'];
} else {
    $errors['straat'] = 'Please enter your street address.';
}

if ($_POST['postcode'] != '') {
    $_POST['postcode'] = filter_var($_POST['postcode'], FILTER_SANITIZE_STRING);

    // Sla de waarde op in een variabele zodat we dat kunnen tonen als er
    // foutmeldingen zijn.
    $zip = $_POST['postcode'];
} else {
    $errors['postcode'] = 'Please enter your zip code.';
}

if ($_POST['gemeente'] != '') {
    $_POST['gemeente'] = filter_var($_POST['postcode'], FILTER_SANITIZE_STRING);

    // Sla de waarde op in een variabele zodat we dat kunnen tonen als er
    // foutmeldingen zijn.
    $city = $_POST['gemeente'];
} else {
    $errors['gemeente'] = 'Please enter your city.';
}

if ($_POST['land'] != '') {
    $_POST['land'] = filter_var($_POST['land'], FILTER_SANITIZE_STRING);

    // Sla de waarde op in een variabele zodat we dat kunnen tonen als er
    // foutmeldingen zijn.
    $country = $_POST['land'];
} else {
    $errors['land'] = 'Please enter your country.';
}

if ($_POST['telefoon'] != '') {
    $_POST['telefoon'] = filter_var($_POST['telefoon'], FILTER_SANITIZE_STRING);

    // Sla de waarde op in een variabele zodat we dat kunnen tonen als er
    // foutmeldingen zijn.
    $phone = $_POST['telefoon'];
} else {
    $errors['telefoon'] = 'Please enter your phone number.';
}


if ($errors === false) {
    // Sla de waarden op in de database.
    header('Location: home.php');
}
}
}




?>
