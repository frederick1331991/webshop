-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Machine: localhost
-- Genereertijd: 28 Mar 2013 om 21:32
-- Serverversie: 5.1.44
-- PHP-Versie: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `webshop`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `members`
--

CREATE TABLE IF NOT EXISTS `members` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `username` varchar(65) NOT NULL DEFAULT '',
  `password` varchar(65) NOT NULL DEFAULT '',
  `naam` varchar(65) NOT NULL DEFAULT '',
  `voornaam` varchar(65) NOT NULL DEFAULT '',
  `adres` varchar(65) NOT NULL DEFAULT '',
  `postcode` varchar(65) NOT NULL DEFAULT '',
  `telefoon` varchar(65) NOT NULL DEFAULT '',
  `email` varchar(65) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Gegevens worden uitgevoerd voor tabel `members`
--

INSERT INTO `members` (`id`, `username`, `password`, `naam`, `voornaam`, `adres`, `postcode`, `telefoon`, `email`) VALUES
(14, 'frederick', 'wxcvbn', 'lemmens', 'frederick', 'schootstraat 19', 'Heusden-Zolder', '3211435181', 'lemmensfrederick@gmail.com');
